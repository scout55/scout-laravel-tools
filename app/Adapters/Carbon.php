<?php


namespace Scout\Laravel\Tools\Adapters;


use Illuminate\Support\Carbon as Base;

class Carbon extends Base
{
    public static function optional($date, string $format = "Y-m-d H:i:s"): ?string
    {
        if (is_null($date)) {
            return null;
        }

        if ($date instanceof Base) {
            return $date->format($format);
        }

        $var = static::make($date);
        if ($var) {
            return $var->format($format);
        }

        return null;
    }
}
