<?php


namespace Scout\Laravel\Tools\Adapters\Resource;


use Illuminate\Http\Resources\Json\JsonResource;
use Scout\Laravel\Tools\Entities\Pagination\PaginationCollection;

class PaginationResource extends JsonResource
{
    public static function paginate(PaginationCollection $collection): array
    {
        return [
            "meta" => [
                "total" => $collection->getTotal(),
                "offset" => $collection->getOffset(),
                "limit" => $collection->getLimit(),
                "page" => [
                    "current" => (int)($collection->getOffset() / $collection->getLimit()) + 1,
                    "total" => ceil($collection->getTotal() / $collection->getLimit()),
                ],
                "sort_by" => $collection->getSortBy(),
                "order_by" => $collection->getOrderBy()
            ],
            "items" => static::collection($collection->getItems()),
        ];
    }
}
