<?php


namespace Scout\Laravel\Tools\Adapters\Response;


use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Arr;

class Factory extends ResponseFactory
{
    public function default(int $code, int $status, int $extcode, string $type, string $message, $data = [], $errors = [])
    {
        $res['status'] = $status;
        $res['code'] = $code;
        $res['extcode'] = $extcode;
        $res['type'] = $type;
        $res['message'] = $message;
        $res['errors'] = is_object($errors) ? $errors : Arr::wrap($errors);
        $res['data'] = is_object($data) ? $data : Arr::wrap($data);

        return $this->json($res)->setStatusCode($status);
    }

    public function ok($params = [])
    {
        return $this->default(0, 200, 0, "success", "", $params, []);
    }


    public function exception(string $message, $errors = [])
    {
        return $this->default(1, 500, 0, "exception", $message, [], $errors);
    }

    public function notFound(?string $message = "")
    {
        return $this->default(1, 404, 0, "exception", $message ?: "Объект не найден", [], []);
    }

    public function valid($errors = [], string $message = "Invalid input.")
    {
        return $this->default(1, 422, 0, "validation", $message, [], $errors);
    }

    public function accepted()
    {
        return $this->default(0, 202, 0, "Accepted", "", [], []);
    }

    public function whops(?string $message = "something went wrong...")
    {
        return $this->default(0, 500, 0, "Server exception", $message, [], []);
    }


    public function png($image, string $filename)
    {
        header('Content-Type: image/png');
        header("Content-Disposition: attachment; filename=$filename");
        imagepng($image);
    }
}
