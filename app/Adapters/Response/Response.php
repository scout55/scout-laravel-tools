<?php


namespace Scout\Laravel\Tools\Adapters\Response;


use Illuminate\Http\JsonResponse;

/**
 * Class Response
 * @package Scout\Laravel\Tools\Adapters\Response
 *
 *
 * @method static JsonResponse default(int $code, int $status, int $extcode, string $type, string $message, $data = [], $errors = [])
 * @method static JsonResponse ok($params = [])
 * @method static JsonResponse exception(string $message, $errors = [])
 * @method static JsonResponse valid($errors = [], string $message = "Invalid input.")
 * @method static JsonResponse notFound(?string $message="")
 * @method static JsonResponse accepted()
 * @method static JsonResponse whops(?string $message="something went wrong...")
 * @method static void png($image, string $filename)
 */
class Response extends \Illuminate\Support\Facades\Response
{

}
