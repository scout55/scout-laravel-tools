<?php


namespace Scout\Laravel\Tools\Entities\Sorting;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class CreatedAtSorting extends Sorting
{
    private string $table;

    public function __construct(string $table)
    {
        $this->table = $table;
    }

    protected function getCodeName(): string
    {
        return 'created_at';
    }

    public function handle(Builder $builder, string $direction = 'desc'): Builder
    {
        return $builder
            ->addSelect(DB::raw(" `{$this->table}`.* "))
            ->addSelect(DB::raw(" `{$this->table}`.`created_at` as `order_by__column` "))
            ->orderBy('order_by__column', $direction);
    }
}
