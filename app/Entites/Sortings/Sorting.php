<?php


namespace Scout\Laravel\Tools\Entities\Sorting;


use Illuminate\Database\Eloquent\Builder;

abstract class Sorting
{
    /**
     * Системное название параметра
     *
     * @return string
     */
    abstract protected function getCodeName(): string;

    abstract public function handle(Builder $builder, string $direction = 'desc'): Builder;

    public function isTruthy(?string $value): bool
    {
        return !is_null($value) && $value === $this->getCodeName();
    }
}
