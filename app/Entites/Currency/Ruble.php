<?php


namespace Scout\Laravel\Tools\Entities\Currency;


class Ruble
{
    private int $kopecks;

    public function __construct($kopecks = 0)
    {
        $this->kopecks = (int)$kopecks;
    }

    public function kopecks(): int
    {
        return $this->kopecks;
    }

    public function rubles(): float
    {
        return $this->kopecks / 100;
    }

    public function addKopecks(int $value)
    {
        $this->kopecks += $value;
    }

    public function addRubles(float $value)
    {
        $this->addKopecks($value * 100);
    }

    public function subKopecks(int $value)
    {
        $this->addKopecks(-$value);
    }

    public function subRubles(float $value)
    {
        $this->addRubles(-$value);
    }

    public function setKopecks(int $value)
    {
        $this->kopecks = $value;
    }

    public function setRubles(float $value)
    {
        $this->setKopecks($value * 100);
    }
}
