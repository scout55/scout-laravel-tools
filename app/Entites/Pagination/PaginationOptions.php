<?php


namespace Scout\Laravel\Tools\Entities\Pagination;



use Scout\Laravel\Tools\Interfaces\Requests\Traits\IWithIndexOptions;

class PaginationOptions implements IWithIndexOptions
{
    private int $limit;
    private int $offset;
    private ?string $sort_by;
    private string $order_by;

    /**
     * PaginationOptions constructor.
     * @param int $limit
     * @param int $offset
     * @param string|null $sort_by
     * @param string $order_by
     */
    public function __construct(int $limit = 500, int $offset = 0, ?string $sort_by = null, string $order_by = 'desc')
    {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->sort_by = $sort_by;
        $this->order_by = $order_by;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return string|null
     */
    public function getSortBy(): ?string
    {
        return $this->sort_by;
    }

    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return $this->order_by;
    }
}
