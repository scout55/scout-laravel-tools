<?php


namespace Scout\Laravel\Tools\Entities\Pagination;


class PaginationCollection
{
    private int $limit;
    private int $offset;
    private int $total;
    private array $items;
    private ?string $sort_by;
    private ?string $order_by;

    /**
     * PaginationCollection constructor.
     * @param int $limit
     * @param int $offset
     * @param int $total
     * @param array $items
     * @param string|null $sort_by
     * @param string|null $order_by
     */
    public function __construct(int $limit, int $offset, int $total, array $items, ?string $sort_by, ?string $order_by)
    {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->total = $total;
        $this->items = $items;
        $this->sort_by = $sort_by;
        $this->order_by = $order_by;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return string|null
     */
    public function getSortBy(): ?string
    {
        return $this->sort_by;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->order_by;
    }
}
