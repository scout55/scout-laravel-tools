<?php


namespace Scout\Laravel\Tools\Traits\Requests;


use Scout\Laravel\Tools\Interfaces\Requests\Traits\ISorting;

/**
 * Trait Pagination
 * @package Scout\Laravel\Tools\Traits\Requests
 *
 * Пришло
 * @property string $sort_by
 * @property string $order_by
 *
 * @see ISorting
 */
trait Sorting
{
    private array $sort_by_in = [];
    private ?string $sort_by_default = null;
    private array $order_by_in = ['asc', 'desc'];
    private string $order_by_default = 'desc';

    private function getSortingRules(): array
    {
        $sort_by = ['nullable', 'string'];
        $order_by = ['nullable', 'string'];

        if(!empty($this->sort_by_in)) $sort_by []= "in:" . implode(',', $this->sort_by_in);
        if(!empty($this->order_by_in)) $order_by []= "in:" . implode(',', $this->order_by_in);

        return [
            'sort_by' => $sort_by,
            'order_by' => $order_by,
        ];
    }

    /**
     * @return string
     */
    public function getSortBy(): ?string
    {
        return (string)$this->sort_by ?: $this->sort_by_default;
    }

    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return (string)$this->order_by ?: $this->order_by_default;
    }
}
