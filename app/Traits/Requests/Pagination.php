<?php


namespace Scout\Laravel\Tools\Traits\Requests;


/**
 * Trait Pagination
 * @package Scout\Laravel\Tools\Traits\Requests
 *
 * Пришло
 * @property $limit
 * @property $offset
 */
trait Pagination
{
    private function getPaginationRules(): array
    {
        return [
            'limit' => ['nullable', 'integer', 'min:0', 'max:10000'],
            'offset' => ['nullable', 'integer', 'min:0']
        ];
    }

    public function getLimit(): int
    {
        return (int)$this->limit ?: 500;
    }

    public function getOffset(): int
    {
        return (int)$this->offset;
    }
}
