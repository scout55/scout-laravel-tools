<?php


namespace Scout\Laravel\Tools\Mutex;


use Illuminate\Support\Facades\Cache;

abstract class CacheMutex implements IMutex
{
    private const KEY = "scout.laravel.mutex.cache:";
    private const LIFETIME_IN_SEC = 60 * 60;

    abstract protected function getKey(): string;

    /*
     * CONTRACT
     */
    public function set()
    {
        Cache::put($this->getFinalKey(), 1, $this->getTTL());
    }

    public function exists(): bool
    {
        return Cache::has($this->getFinalKey());
    }

    public function clear()
    {
        Cache::forget($this->getFinalKey());
    }

    /*
     * METHODS
     */
    private function getFinalKey(): string
    {
        return self::KEY.$this->getKey();
    }

    protected function getTTL(): int
    {
        return self::LIFETIME_IN_SEC;
    }
}
