<?php


namespace Scout\Laravel\Tools\Mutex;

interface IMutex
{
    public function set();

    public function exists(): bool;

    public function clear();
}
