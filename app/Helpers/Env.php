<?php

if (!function_exists('isProd')) {
    /**
     * @return bool
     */
    function isProd()
    {
        return config('app.env', 'production') == 'production';
    }
}

if (!function_exists('isLocal')) {
    /**
     * @return bool
     */
    function isLocal()
    {
        return config('app.env', 'production') == 'local';
    }
}

if (!function_exists('isStage')) {
    /**
     * @return bool
     */
    function isStage()
    {
        return config('app.env', 'production') == 'stage';
    }
}
