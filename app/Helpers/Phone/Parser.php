<?php


namespace Scout\Laravel\Tools\Phone;


interface Parser
{
    public function parse(string $phone): string;

    public function valid(string $phone): bool;
}
