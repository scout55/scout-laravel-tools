<?php


namespace Scout\Laravel\Tools\Phone;


class RussianParser implements Parser
{
    private const PHONE_LENGTH = 11;
    private const CODES = ["8", "7"];

    public function parse(string $phone): string
    {
        $str = $this->clear($phone);
        $str = "8" . substr($str, 1);

        return $str;
    }

    public function valid(string $phone): bool
    {
        $str = $this->clear($phone);

        return $this->isCorrectLength($str) && $this->isRightCode($str);
    }

    private function clear(string $phone): string
    {
        return preg_replace("/\D/u", "", $phone);
    }

    private function isCorrectLength(string $phone)
    {
        return strlen($phone) === self::PHONE_LENGTH;
    }

    private function isRightCode(string $phone)
    {
        foreach (self::CODES as $code) {
            if ($code == $phone[0]) return true;
        }
        return false;
    }

}
