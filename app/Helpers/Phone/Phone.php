<?php


namespace Scout\Laravel\Tools\Phone;

class Phone
{
    private static $singletons = [];

    private static function singleton(string $class)
    {
        if (!isset(static::$singletons[$class])) {
            static::$singletons[$class] = new $class();
        }

        return static::$singletons[$class];
    }

    /**
     * @return RussianParser
     */
    public static function ru(): RussianParser
    {
        return self::singleton(RussianParser::class);
    }
}
