<?php


namespace Scout\Laravel\Tools\Helpers;


use Illuminate\Support\Facades\Log;

class Sentry
{
    public static function capture(\Throwable $exception)
    {
        if (app()->bound('sentry') && isProd()) {

            // @var Sentry\Client $client
            $client = app('sentry');

            $client->captureException($exception);

            return;

        }

        Log::debug("SENT TO SENTRY: " . $exception->getMessage());
    }
}
