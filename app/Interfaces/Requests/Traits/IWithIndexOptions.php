<?php


namespace Scout\Laravel\Tools\Interfaces\Requests\Traits;


interface IWithIndexOptions extends IPagination, ISorting
{
}
