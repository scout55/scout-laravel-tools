<?php


namespace Scout\Laravel\Tools\Interfaces\Requests\Traits;


interface IPagination
{
    public function getLimit(): int;

    public function getOffset(): int;
}
