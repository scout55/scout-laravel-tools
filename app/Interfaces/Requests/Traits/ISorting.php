<?php


namespace Scout\Laravel\Tools\Interfaces\Requests\Traits;


interface ISorting
{
    /** column */
    public function getSortBy(): ?string;

    /** direction  */
    public function getOrderBy(): string;
}
