<?php


namespace Scout\Laravel\Tools\Pipelines\Builder;


use Illuminate\Database\Eloquent\Builder;
use Scout\Laravel\Tools\Entities\Sorting\Sorting;
use Scout\Laravel\Tools\Interfaces\Requests\Traits\ISorting;

class Sort
{
    private ISorting $sorting;
    private array $handlers;

    /**
     * Sort constructor.
     * @param ISorting $sorting
     * @param Sorting[] $handlers
     */
    public function __construct(ISorting $sorting, array $handlers = [])
    {
        $this->sorting = $sorting;
        $this->handlers = $handlers;
    }


    public function handle(Builder $builder, $next)
    {
        if (!empty($this->sorting->getSortBy()) && !empty($this->handlers)) {
            foreach ($this->handlers as $handler) {
                if ($handler->isTruthy($this->sorting->getSortBy())) {
                    $builder = $handler->handle($builder, $this->sorting->getOrderBy());
                    break;
                }
            }
        }

        return $next($builder);
    }
}
