<?php


namespace Scout\Laravel\Tools\Pipelines\Builder;


use Illuminate\Database\Eloquent\Builder;
use Scout\Laravel\Tools\Interfaces\Requests\Traits\IPagination;

class Paginate
{
    private IPagination $pagination;

    /**
     * Paginate constructor.
     * @param IPagination $pagination
     */
    public function __construct(IPagination $pagination)
    {
        $this->pagination = $pagination;
    }


    public function handle(Builder $builder, $next)
    {
        $builder
            ->limit($this->pagination->getLimit())
            ->offset($this->pagination->getOffset());

        return $next($builder);
    }
}
